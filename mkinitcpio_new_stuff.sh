#make sure /dev /proc and /sys are mounted or larchify won't work right. RUN THIS EVERY TIME
# you reboot
#!/bin/bash
echo mounting /proc /sys and /dev for the larch build:
LARCHBUILD=$HOME/Documents/larch_build
sudo mount -o bind /dev $LARCHBUILD/dev

sudo chroot $LARCHBUILD && mount -t sysfs sys /sys && exit
sudo chroot $LARCHBUILD && mount -t proc proc /proc && exit
