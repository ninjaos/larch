=LARCH - Live ARCH=
This is the LARCH live cd/usb constructor kit for making a Live OS based on
Arch Linux. It was originally written by GradGrind as a college project, but
was since handed over to the Ninja OS development team. The original page:

http://larch.berlios.de

is long since defunct as berlios.de itself has been abandoned.

This version of Larch gets bug fixes to keep it working for the sake of keeping
Ninja OS going, and is provided here as the build tool needed to "compile"
Ninja OS, as it is not available elsewhere anymore.

the Development Ninja has no plans to maintain this long term, and it already
shows many signs of bit-rot. Some features do not work, many features are based
on old vers of Arch.

It is not recommended you start any new projects using larch. It is recommended
you find another tool chain.

Ninja OS is investigating new tool chains, and 0.11.5 will be the last version
based on larch. After that, chances of bug fixes are slim to none.
