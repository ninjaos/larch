Experimental re-packaging of pacman-allin by grad-grind from larch. Functions as a drop in replacement. This is a port to pacman 4. use this with larch to keep working with modern repos.

So far gpg keys don't work. larch needs a re-write to use gpg keys. so put this line in your pacman.conf associated with your project for now:

SigLevel = Never
