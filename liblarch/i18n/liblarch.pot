# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR ORGANIZATION
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-01-16 05:18+CET\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING\n"
"Generated-By: pygettext.py 1.5\n"


#: rootrun.py:123
msgid "Please enter (sudo) password:"
msgstr ""

#: rootrun.py:130
msgid "Please enter root password:"
msgstr ""

#: rootrun.py:150
msgid "Operation cancelled"
msgstr ""

#: rootrun.py:163
msgid "Incorrect password, try again:"
msgstr ""

#: translation.py:60
msgid "Document '%s' not found"
msgstr ""

#: translation.py:68
msgid "This translation is not up-to-date, see %s for the original, untranslated version."
msgstr ""

#: uim/larcon.uim:62
msgid "Quit"
msgstr ""

#: uim/larcon.uim:63
msgid "Exit the application, make no (further) changes"
msgstr ""

#: uim/larcon.uim:86
msgid "Help"
msgstr ""

#: uim/larcon.uim:87
msgid "Show the help page"
msgstr ""

#: uim/larcon.uim:88
msgid "Hide help"
msgstr ""

#: uim/larcon.uim:89
msgid "Hide the help page, return to main view"
msgstr ""

