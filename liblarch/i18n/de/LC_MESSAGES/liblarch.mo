��          �      �       0     1  /   I     y  	   ~  '   �     �     �     �               "  R   5  R  �     �  B   �     ;     A  8   W  (   �     �     �     �  
          W   0                      
                          	          Document '%s' not found Exit the application, make no (further) changes Help Hide help Hide the help page, return to main view Incorrect password, try again: Operation cancelled Please enter (sudo) password: Please enter root password: Quit Show the help page This translation is not up-to-date, see %s for the original, untranslated version. Project-Id-Version: liblarch-1
POT-Creation-Date: 2011-01-16 05:14+CET
PO-Revision-Date: 2011-01-16
Last-Translator: Automatically generated
Language-Team: none
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 DOkument '%s' nicht gefunden Schließe die Anwendung, keine (weiteren) Änderungen durchführen Hilfe Hilfsseite schließen Verstecke die Hilfsseite, kehre zur Hauptansicht zurück Falsches Passwort, noch einmal versuchen Operation abgebrochen Bitte (sudo) Passwort eingeben: Bitte root-Passwort eingeben: Schließen Zeige die Hilfsseite an Diese Übersetzung ist nicht aktuell, siehe %s für die nicht übersetzte Quellversion. 