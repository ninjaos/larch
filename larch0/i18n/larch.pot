# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR ORGANIZATION
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-02-23 19:54+CET\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING\n"
"Generated-By: pygettext.py 1.5\n"


#: cli/archin.py:38
msgid "Operations on '/' are not supported ..."
msgstr ""

#: cli/archin.py:61
msgid "No pacman executable found"
msgstr ""

#: cli/archin.py:143
msgid "Install Arch to '%s'?"
msgstr ""

#: cli/archin.py:157
msgid "Couldn't write to the installation path (%s)"
msgstr ""

#: cli/archin.py:161
msgid "The installation path (%s) is mounted 'nodev'."
msgstr ""

#: cli/archin.py:168
msgid "The installation path (%s) is mounted 'noexec'."
msgstr ""

#: cli/archin.py:193
msgid "Package installation failed"
msgstr ""

#: cli/archin.py:200
msgid "Arch installation completed"
msgstr ""

#: cli/archin.py:207
msgid "No '%s' file"
msgstr ""

#: cli/archin.py:221
msgid "Invalid package file include: %s"
msgstr ""

#: cli/archin.py:253
msgid "Couldn't synchronize pacman database (pacman -Sy)"
msgstr ""

#: cli/archin.py:316
msgid "usage: %%prog [options] %s [packages]"
msgstr ""

#: cli/archin.py:321 cli/larchify.py:581 cli/medium.py:189
msgid "Profile: 'user:profile-name' or path to profile directory"
msgstr ""

#: cli/archin.py:324 cli/larchify.py:584
msgid "Path to directory to be larchified (default %s)"
msgstr ""

#: cli/archin.py:327 cli/larchify.py:587 cli/medium.py:227
msgid "Run as a slave from a controlling program (e.g. from a gui)"
msgstr ""

#: cli/archin.py:330 cli/larchify.py:590 cli/medium.py:224
msgid "Suppress output messages, except errors (no effect if -s specified)"
msgstr ""

#: cli/archin.py:335 cli/larchify.py:598
msgid "Don't ask for confirmation"
msgstr ""

#: cli/archin.py:339
msgid "Supply a substitute repository list (pacman.conf.repos) for the installation only"
msgstr ""

#: cli/archin.py:343
msgid "pacman cache directory (default /var/cache/pacman/pkg)"
msgstr ""

#: cli/archin.py:346
msgid "Don't show pacman's progress bar"
msgstr ""

#: cli/archin.py:358
msgid ""
"You must specify which operation to perform:\n"
msgstr ""

#: cli/archin.py:363 cli/larchify.py:603 cli/medium.py:274
msgid "This application must be run as root"
msgstr ""

#: cli/archin.py:369
msgid ""
"Invalid operation: '%s'\n"
msgstr ""

#: cli/backend.py:79
msgid ""
"larch (%s) seems to be running already.\n"
"If you are absolutely sure this is not the case,\n"
"you may continue. Otherwise you should cancel.\n"
"\n"
"Shall I continue?"
msgstr ""

#: cli/backend.py:129
msgid "The backend reported %d failed calls, you may want to investigate"
msgstr ""

#: cli/backend.py:149
msgid "Yes:y|No:n"
msgstr ""

#: cli/backend.py:181
msgid ""
"Something went wrong:\n"
msgstr ""

#: cli/backend.py:312
msgid "Invalid profile: %s"
msgstr ""

#: cli/backend.py:315
msgid "Invalid profile folder: %s"
msgstr ""

#: cli/backend.py:440
msgid "Couldn't read file: %s"
msgstr ""

#: cli/larchify.py:46
msgid ""
"File '%s' doesn't exist:\n"
"  '%s' not an Arch installation?"
msgstr ""

#: cli/larchify.py:54
msgid ""
"Building a larch live medium from the running system is\n"
"an error prone process. Changes to the running system\n"
"made while running this function may be only partially\n"
"incorporated into the compressed system images.\n"
"\n"
"Do you wish to continue?"
msgstr ""

#: cli/larchify.py:134
msgid "Squashing system.sqf failed"
msgstr ""

#: cli/larchify.py:151
msgid "Warning: /boot in the profile's overlay will not be used"
msgstr ""

#: cli/larchify.py:236
msgid "Build customization script failed"
msgstr ""

#: cli/larchify.py:259 cli/medium.py:105
msgid "Squashing mods.sqf failed"
msgstr ""

#: cli/larchify.py:265
msgid "larchify-process completed"
msgstr ""

#: cli/larchify.py:369
msgid "%d user account operation(s) failed"
msgstr ""

#: cli/larchify.py:390
msgid ""
"No squashfs module found\n"
msgstr ""

#: cli/larchify.py:402
msgid ""
"No aufs or unionfs module found\n"
msgstr ""

#: cli/larchify.py:406
msgid ""
"Package '%s' is needed by larch systems\n"
msgstr ""

#: cli/larchify.py:409
msgid ""
"Without package 'syslinux' you will not be able\n"
"to create syslinux or isolinux booting media\n"
msgstr ""

#: cli/larchify.py:413
msgid ""
"Without package 'cdrkit' (or 'cdrtools') you will\n"
"not be able to create CD/DVD media\n"
msgstr ""

#: cli/larchify.py:417
msgid ""
"Without package 'eject' you will have problems\n"
"using CD/DVD media\n"
msgstr ""

#: cli/larchify.py:421
msgid ""
"WARNING:\n"
"%s\n"
"    Continue building?"
msgstr ""

#: cli/larchify.py:427
msgid ""
"ERROR:\n"
"%s"
msgstr ""

#: cli/larchify.py:461
msgid "Kernel file '%s' not found"
msgstr ""

#: cli/larchify.py:480
msgid "No mkinitcpio preset file (%s)"
msgstr ""

#: cli/larchify.py:516
msgid ""
"WARNING:\n"
"  You seem to have installed a package containing modules\n"
"which aren't compatible with your kernel (see log).\n"
"Please check that this won't cause problems.\n"
"Maybe you need the corresponding package for your kernel?\n"
"\n"
"    Continue building?"
msgstr ""

#: cli/larchify.py:533
msgid "Couldn't find kernel modules"
msgstr ""

#: cli/larchify.py:577
msgid "usage: %prog [options]"
msgstr ""

#: cli/larchify.py:593
msgid "Reuse previously generated system.sqf"
msgstr ""

#: cli/larchify.py:596
msgid "Reuse previously generated locales"
msgstr ""

#: cli/media_common.py:62
msgid "Invalid source medium: '%s'"
msgstr ""

#: cli/media_common.py:115
msgid "No kernel and/or initramfs"
msgstr ""

#: cli/media_common.py:173
msgid "No loader binary, "
msgstr ""

#: cli/media_common.py:200
msgid "Invalid output device: %s"
msgstr ""

#: cli/media_common.py:224
msgid "Couldn't get format information for %s"
msgstr ""

#: cli/media_common.py:233
msgid "Unsupported file-system: %s"
msgstr ""

#: cli/media_common.py:276
msgid "Couldn't format %s"
msgstr ""

#: cli/media_common.py:303
msgid "Couldn't mount larch partition, %s"
msgstr ""

#: cli/media_common.py:333
msgid "iso build failed"
msgstr ""

#: cli/media_common.py:334
msgid "%s was successfully created"
msgstr ""

#: cli/media_common.py:344
msgid "File '%s' doesn't exist, '%s' is not a larch medium"
msgstr ""

#: cli/media_common.py:360
msgid "The volume label is too long. Use the default (%s)?"
msgstr ""

#: cli/media_common.py:364
msgid "Cancelled"
msgstr ""

#: cli/media_common.py:384
msgid "Can't boot to label - device has no label"
msgstr ""

#: cli/media_common.py:395
msgid "Boot configuration file '%s' not found"
msgstr ""

#: cli/media_common.py:434
msgid "Base configuration file (%s) not found"
msgstr ""

#: cli/medium.py:74
msgid "Couldn't find boot configuration file"
msgstr ""

#: cli/medium.py:86
msgid ""
"Copying of devices with both 'overlay.medium' and 'mods.sqf'\n"
"is not supported."
msgstr ""

#: cli/medium.py:117
msgid "Unpacking of modifications archive failed, see log"
msgstr ""

#: cli/medium.py:152
msgid "Completed writing to %s"
msgstr ""

#: cli/medium.py:171
msgid "usage: %prog [options] [partition (e.g. /dev/sdb1)]"
msgstr ""

#: cli/medium.py:175
msgid "Specify source medium: base directory (mount point) starting '/', an iso-file (path ending '.iso'), device (starting '/dev/') or volume label"
msgstr ""

#: cli/medium.py:180
msgid "Volume label for boot medium (default: %s - or %s if boot iso)"
msgstr ""

#: cli/medium.py:184
msgid "Build a boot iso for the source partition"
msgstr ""

#: cli/medium.py:192
msgid "Path to larchified directory (default %s)"
msgstr ""

#: cli/medium.py:197
msgid "Specify the output file (default: '%s' in the current directory - or '%s' if boot iso)"
msgstr ""

#: cli/medium.py:203
msgid "Method for boot partition detection: %s (default: label)"
msgstr ""

#: cli/medium.py:207
msgid "Don't generate 'larch/larchboot' file"
msgstr ""

#: cli/medium.py:210
msgid "Don't format the medium (WARNING: Only for experts)"
msgstr ""

#: cli/medium.py:213
msgid "Enable data persistence (using medium as writeable file-system). Default: disabled"
msgstr ""

#: cli/medium.py:217
msgid "Don't install the bootloader (to the MBR)"
msgstr ""

#: cli/medium.py:219
msgid "Don't use journalling on boot medium (default: journalling enabled)"
msgstr ""

#: cli/medium.py:231
msgid "Test source or destination medium only (used by gui)"
msgstr ""

#: cli/medium.py:239
msgid "Unexpected argument: %s"
msgstr ""

#: cli/medium.py:241
msgid "No source specified for boot iso"
msgstr ""

#: cli/medium.py:246
msgid ""
"Generating larch boot iso file: %s\n"
msgstr ""

#: cli/medium.py:252
msgid ""
"Generating larch iso file: %s\n"
msgstr ""

#: cli/medium.py:261
msgid ""
"Testing output medium: %s\n"
msgstr ""

#: cli/medium.py:263
msgid ""
"Creating larch medium on: %s\n"
msgstr ""

#: cli/medium.py:265
msgid "Invalid partition: '%s'"
msgstr ""

#: cli/medium.py:268
msgid ""
"Testing source medium: %s\n"
msgstr ""

#: cli/userinfo.py:45
msgid "Invalid 'users' file"
msgstr ""

#: cli/userinfo.py:67
msgid "Couldn't add user '%s'"
msgstr ""

#: cli/userinfo.py:75
msgid "Couldn't remove user '%s'"
msgstr ""

#: cli/userinfo.py:90
msgid "Couldn't save 'users' file"
msgstr ""

#: gui/controller.py:120
msgid "Couldn't read file '%s'"
msgstr ""

#: gui/controller.py:133
msgid "Couldn't save file '%s'"
msgstr ""

#: gui/front/page_larchify.py:63
msgid "No Arch installation at %s"
msgstr ""

#: gui/front/page_larchify.py:270
msgid "Invalid kernel binary: %s"
msgstr ""

#: gui/front/page_larchify.py:287
msgid "Invalid kernel mkinitcpio preset: %s"
msgstr ""

#: gui/front/page_project.py:131
msgid "Name for new profile:"
msgstr ""

#: gui/layouts/docviewer.uim:35
msgid "Documentation"
msgstr ""

#: gui/layouts/docviewer.uim:40 gui/layouts/logger.uim:52
msgid "Hide"
msgstr ""

#: gui/layouts/docviewer.uim:41
msgid "Return to the larch controls"
msgstr ""

#: gui/layouts/docviewer.uim:47
msgid "Go back in the viewing history"
msgstr ""

#: gui/layouts/docviewer.uim:53
msgid "Go forward in the viewing history"
msgstr ""

#: gui/layouts/docviewer.uim:60
msgid "Reload the documentation for the current larch tab"
msgstr ""

#: gui/layouts/docviewer.uim:67
msgid "Go to the general larch documentation index"
msgstr ""

#: gui/layouts/editor.uim:39
msgid "Editor"
msgstr ""

#: gui/layouts/editor.uim:45
msgid "OK"
msgstr ""

#: gui/layouts/editor.uim:50 gui/layouts/progress.uim:54
msgid "Cancel"
msgstr ""

#: gui/layouts/editor.uim:55
msgid "Revert"
msgstr ""

#: gui/layouts/editor.uim:56
msgid "Restore the text to its initial/default state"
msgstr ""

#: gui/layouts/editor.uim:61
msgid "Copy"
msgstr ""

#: gui/layouts/editor.uim:66
msgid "Cut"
msgstr ""

#: gui/layouts/editor.uim:71
msgid "Paste"
msgstr ""

#: gui/layouts/editor.uim:76
msgid "Undo"
msgstr ""

#: gui/layouts/editor.uim:81
msgid "Redo"
msgstr ""

#: gui/layouts/editor.uim:88
msgid "Editing '%s'"
msgstr ""

#: gui/layouts/logger.uim:37
msgid "Low-level Command Logging"
msgstr ""

#: gui/layouts/logger.uim:38 gui/layouts/progress.uim:39
msgid "Here you can follow the detailed, low-level progress of the commands."
msgstr ""

#: gui/layouts/logger.uim:47
msgid "Clear"
msgstr ""

#: gui/layouts/logger.uim:53
msgid "Go back to the larch controls"
msgstr ""

#: gui/layouts/page_installation.uim:42
msgid "Edit Profile"
msgstr ""

#: gui/layouts/page_installation.uim:53
msgid "Edit 'addedpacks'"
msgstr ""

#: gui/layouts/page_installation.uim:54
msgid "Edit the list of packages to be installed"
msgstr ""

#: gui/layouts/page_installation.uim:59
msgid "Edit 'vetopacks'"
msgstr ""

#: gui/layouts/page_installation.uim:60
msgid "Edit the list of packages NOT to install"
msgstr ""

#: gui/layouts/page_installation.uim:65
msgid "Edit pacman.conf options"
msgstr ""

#: gui/layouts/page_installation.uim:66
msgid "Edit pacman.conf options - not the repositories"
msgstr ""

#: gui/layouts/page_installation.uim:71
msgid "Edit pacman.conf repositories"
msgstr ""

#: gui/layouts/page_installation.uim:72
msgid "Edit the repository entries for pacman.conf"
msgstr ""

#: gui/layouts/page_installation.uim:79
msgid "Tweak Installed Packages"
msgstr ""

#: gui/layouts/page_installation.uim:85
msgid "Synchronize db    [-Sy]"
msgstr ""

#: gui/layouts/page_installation.uim:86
msgid "Synchronize the pacman db on the target (pacman -Sy)"
msgstr ""

#: gui/layouts/page_installation.uim:91
msgid "Update all packages    [-Su]"
msgstr ""

#: gui/layouts/page_installation.uim:92
msgid ""
"Update all installed packages for which a newer version is available\n"
"(you will normally need to synchronize the pacman db first)"
msgstr ""

#: gui/layouts/page_installation.uim:98
msgid "Update / Add package    [-U]"
msgstr ""

#: gui/layouts/page_installation.uim:99
msgid "Update / Add a package from a package file using pacman -U"
msgstr ""

#: gui/layouts/page_installation.uim:105
msgid "Add package(s)    [-S]"
msgstr ""

#: gui/layouts/page_installation.uim:106
msgid "Add one or more packages (space separated) using pacman -S"
msgstr ""

#: gui/layouts/page_installation.uim:112
msgid "Remove package(s)    [-Rs]"
msgstr ""

#: gui/layouts/page_installation.uim:113
msgid "Remove one or more packages (space separated) using pacman -Rs"
msgstr ""

#: gui/layouts/page_installation.uim:121
msgid "Advanced Installation Options"
msgstr ""

#: gui/layouts/page_installation.uim:127
msgid "Use project repository list"
msgstr ""

#: gui/layouts/page_installation.uim:128
msgid "Enables use of an alternative pacman.conf for installation only"
msgstr ""

#: gui/layouts/page_installation.uim:136
msgid "Edit repository list"
msgstr ""

#: gui/layouts/page_installation.uim:137
msgid "Edit repository list file used for installation"
msgstr ""

#: gui/layouts/page_installation.uim:142
msgid "Edit mirror list"
msgstr ""

#: gui/layouts/page_installation.uim:143
msgid ""
"Edit the pacman mirror list for the live system\n"
"(not for the build process)"
msgstr ""

#: gui/layouts/page_installation.uim:150
msgid "Package Cache"
msgstr ""

#: gui/layouts/page_installation.uim:157
msgid "The path to the (host's) package cache"
msgstr ""

#: gui/layouts/page_installation.uim:161 gui/layouts/page_larchify.uim:80
#: gui/layouts/page_larchify.uim:98 gui/layouts/page_larchify.uim:190
#: gui/layouts/page_medium.uim:200 gui/layouts/page_project.uim:151
#: gui/layouts/profile_browse.uim:84
msgid "Change"
msgstr ""

#: gui/layouts/page_installation.uim:162
msgid "Change the package cache path"
msgstr ""

#: gui/layouts/page_installation.uim:168
msgid "Install"
msgstr ""

#: gui/layouts/page_installation.uim:169
msgid "This will start the installation to the set path"
msgstr ""

#: gui/layouts/page_installation.uim:176
msgid "Editing pacman.conf options only"
msgstr ""

#: gui/layouts/page_installation.uim:177
msgid "Editing pacman repositories"
msgstr ""

#: gui/layouts/page_installation.uim:178
msgid "Editing mirror list for installation"
msgstr ""

#: gui/layouts/page_installation.uim:179
msgid "Enter new package cache path:"
msgstr ""

#: gui/layouts/page_installation.uim:180
msgid "Editing pacman repositories for installation"
msgstr ""

#: gui/layouts/page_installation.uim:181
msgid "Package to add/update"
msgstr ""

#: gui/layouts/page_installation.uim:182
msgid "Packages"
msgstr ""

#: gui/layouts/page_installation.uim:183
msgid ""
"Enter the names of packages to install -\n"
"  separated by spaces:"
msgstr ""

#: gui/layouts/page_installation.uim:185
msgid ""
"Enter the names of packages to remove -\n"
"  separated by spaces:"
msgstr ""

#: gui/layouts/page_larchify.uim:46
msgid "Supported locales"
msgstr ""

#: gui/layouts/page_larchify.uim:47
msgid "Edit the /etc/locale.gen file to select supported glibc locales"
msgstr ""

#: gui/layouts/page_larchify.uim:53
msgid "Edit /etc/rc.conf"
msgstr ""

#: gui/layouts/page_larchify.uim:54
msgid "Edit the general system configuration file for the live system"
msgstr ""

#: gui/layouts/page_larchify.uim:60
msgid "Edit overlay"
msgstr ""

#: gui/layouts/page_larchify.uim:61
msgid "Open a file browser on the profile's 'rootoverlay'"
msgstr ""

#: gui/layouts/page_larchify.uim:68
msgid "Live kernel filename"
msgstr ""

#: gui/layouts/page_larchify.uim:69
msgid "The name of the kernel binary file (in /boot)"
msgstr ""

#: gui/layouts/page_larchify.uim:81
msgid "Change the name of the kernel binary file (in /boot)"
msgstr ""

#: gui/layouts/page_larchify.uim:86
msgid "Live kernel package"
msgstr ""

#: gui/layouts/page_larchify.uim:87
msgid "The name of the kernel for mkinitcpio (the preset file)"
msgstr ""

#: gui/layouts/page_larchify.uim:99
msgid "Change the name of the kernel preset file (for mkinitcpio)"
msgstr ""

#: gui/layouts/page_larchify.uim:106
msgid "Advanced Options"
msgstr ""

#: gui/layouts/page_larchify.uim:112
msgid "Edit mkinitcpio.conf"
msgstr ""

#: gui/layouts/page_larchify.uim:113
msgid "Edit the configuration file for generating the initramfs via mkinitcpio"
msgstr ""

#: gui/layouts/page_larchify.uim:119
msgid "Generate ssh keys"
msgstr ""

#: gui/layouts/page_larchify.uim:120
msgid "The ssh host keys will be pre-generated"
msgstr ""

#: gui/layouts/page_larchify.uim:125
msgid "Reuse existing locales"
msgstr ""

#: gui/layouts/page_larchify.uim:126
msgid "To save time it may be possible to reuse glibc locales from a previous run"
msgstr ""

#: gui/layouts/page_larchify.uim:133
msgid "Reuse existing system.sqf"
msgstr ""

#: gui/layouts/page_larchify.uim:134
msgid "Reuse existing system.sqf, to save time if the base system hasn't changed"
msgstr ""

#: gui/layouts/page_larchify.uim:140 gui/layouts/page_main.uim:85
msgid "Larchify"
msgstr ""

#: gui/layouts/page_larchify.uim:141
msgid "Build the main components of the larch system"
msgstr ""

#: gui/layouts/page_larchify.uim:148
msgid "User accounts"
msgstr ""

#: gui/layouts/page_larchify.uim:162
msgid "Click on a row to select, click on a selected cell to edit"
msgstr ""

#: gui/layouts/page_larchify.uim:169
msgid "Add user"
msgstr ""

#: gui/layouts/page_larchify.uim:170
msgid "Create a new user-name"
msgstr ""

#: gui/layouts/page_larchify.uim:175
msgid "Delete user"
msgstr ""

#: gui/layouts/page_larchify.uim:176
msgid "Remove the selected user-name"
msgstr ""

#: gui/layouts/page_larchify.uim:181
msgid "Root password:"
msgstr ""

#: gui/layouts/page_larchify.uim:186
msgid "The unencrypted root password for the live system"
msgstr ""

#: gui/layouts/page_larchify.uim:191
msgid "Enter a new password for the 'root' user"
msgstr ""

#: gui/layouts/page_larchify.uim:200
msgid "Renaming failed, see log"
msgstr ""

#: gui/layouts/page_larchify.uim:201
msgid "Couldn't adjust user definition"
msgstr ""

#: gui/layouts/page_larchify.uim:202
msgid "Default (/etc/skel)"
msgstr ""

#: gui/layouts/page_larchify.uim:203
msgid ""
"This folder will be copied\n"
"to build the user's home folder:"
msgstr ""

#: gui/layouts/page_larchify.uim:205
msgid "Choose 'skel' Folder"
msgstr ""

#: gui/layouts/page_larchify.uim:206
msgid "Enter login-name for new user:"
msgstr ""

#: gui/layouts/page_larchify.uim:207
msgid "Enter root password for live system:"
msgstr ""

#: gui/layouts/page_larchify.uim:208
msgid "Name of kernel binary:"
msgstr ""

#: gui/layouts/page_larchify.uim:209
msgid "Name of kernel mkinitcpio preset:"
msgstr ""

#: gui/layouts/page_larchify.uim:214
msgid "Group"
msgstr ""

#: gui/layouts/page_larchify.uim:214
msgid "Password"
msgstr ""

#: gui/layouts/page_larchify.uim:214
msgid "User-Name"
msgstr ""

#: gui/layouts/page_larchify.uim:215
msgid "'skel' directory"
msgstr ""

#: gui/layouts/page_larchify.uim:216
msgid "Additional Groups"
msgstr ""

#: gui/layouts/page_larchify.uim:216
msgid "Expert options"
msgstr ""

#: gui/layouts/page_main.uim:50
msgid "Live Arch Linux Construction Kit"
msgstr ""

#: gui/layouts/page_main.uim:54
msgid "View Log"
msgstr ""

#: gui/layouts/page_main.uim:55
msgid "This button switches to the log viewer"
msgstr ""

#: gui/layouts/page_main.uim:60
msgid "Help"
msgstr ""

#: gui/layouts/page_main.uim:61
msgid "This button switches to the documentation viewer"
msgstr ""

#: gui/layouts/page_main.uim:66
msgid "Quit"
msgstr ""

#: gui/layouts/page_main.uim:67
msgid "Stop the current action and quit the program"
msgstr ""

#: gui/layouts/page_main.uim:83
msgid "Project Settings"
msgstr ""

#: gui/layouts/page_main.uim:84
msgid "Installation"
msgstr ""

#: gui/layouts/page_main.uim:86
msgid "Make Medium"
msgstr ""

#: gui/layouts/page_main.uim:94
msgid "Authentication failure"
msgstr ""

#: gui/layouts/page_main.uim:95
msgid "Enter the password to run as administrator:"
msgstr ""

#: gui/layouts/page_medium.uim:39
msgid "Source of the larch system"
msgstr ""

#: gui/layouts/page_medium.uim:40
msgid ""
"Normally the larchified Arch install,\n"
"but it could also be an existing larch medium"
msgstr ""

#: gui/layouts/page_medium.uim:49
msgid "Select where the system to put on the medium comes from"
msgstr ""

#: gui/layouts/page_medium.uim:56
msgid "The larch data comes from here"
msgstr ""

#: gui/layouts/page_medium.uim:60 gui/layouts/page_medium.uim:116
msgid "Choose"
msgstr ""

#: gui/layouts/page_medium.uim:61
msgid "Select the source of the larch system"
msgstr ""

#: gui/layouts/page_medium.uim:67
msgid "Type of medium to build"
msgstr ""

#: gui/layouts/page_medium.uim:68
msgid "A writeable medium could be a USB-stick or a hard drive partition, for example"
msgstr ""

#: gui/layouts/page_medium.uim:76
msgid "Select the larch source."
msgstr ""

#: gui/layouts/page_medium.uim:82
msgid "Medium profile"
msgstr ""

#: gui/layouts/page_medium.uim:83
msgid "Settings which are saved in the profile"
msgstr ""

#: gui/layouts/page_medium.uim:89
msgid "Edit boot entries"
msgstr ""

#: gui/layouts/page_medium.uim:90
msgid "Edit the file determining the boot entries"
msgstr ""

#: gui/layouts/page_medium.uim:95
msgid "Edit bootloader template"
msgstr ""

#: gui/layouts/page_medium.uim:96
msgid ""
"Edit the syslinux/extlinux/isolinux configuration file\n"
"(the larch boot entries are handled separately)"
msgstr ""

#: gui/layouts/page_medium.uim:102
msgid "Edit medium files"
msgstr ""

#: gui/layouts/page_medium.uim:103
msgid "Open a file browser on the profile's 'cd-root' folder"
msgstr ""

#: gui/layouts/page_medium.uim:112
msgid "The partition or iso-file to receive the larch system"
msgstr ""

#: gui/layouts/page_medium.uim:117
msgid "Select the destination medium or file"
msgstr ""

#: gui/layouts/page_medium.uim:123
msgid "Medium options"
msgstr ""

#: gui/layouts/page_medium.uim:124
msgid "Optional settings for larch partitions"
msgstr ""

#: gui/layouts/page_medium.uim:137
msgid "Format medium"
msgstr ""

#: gui/layouts/page_medium.uim:138
msgid ""
"Normally the medium should be formatted before writing, but IF YOU\n"
"KNOW WHAT YOU ARE DOING it might be useful to skip the formatting"
msgstr ""

#: gui/layouts/page_medium.uim:144
msgid "Don't set MBR"
msgstr ""

#: gui/layouts/page_medium.uim:145
msgid ""
"The bootloader will be installed to the partition only, leaving the mbr untouched\n"
"(you'll need to provide some other means of booting)"
msgstr ""

#: gui/layouts/page_medium.uim:152
msgid "Not bootable via search"
msgstr ""

#: gui/layouts/page_medium.uim:153
msgid ""
"Don't create the file 'larch/larchboot':\n"
" the medium will only be bootable by uuid, label or partition name"
msgstr ""

#: gui/layouts/page_medium.uim:160
msgid "Data persistence"
msgstr ""

#: gui/layouts/page_medium.uim:161
msgid "Support data persistence by using the medium as a writeable overlay"
msgstr ""

#: gui/layouts/page_medium.uim:166
msgid "Use journalling"
msgstr ""

#: gui/layouts/page_medium.uim:167
msgid ""
"The file-system containing the overlay can use journalling\n"
"to aid data integrity"
msgstr ""

#: gui/layouts/page_medium.uim:174
msgid "Medium Detection:"
msgstr ""

#: gui/layouts/page_medium.uim:178
msgid "Choose how the boot scripts determine where to look for the larch system"
msgstr ""

#: gui/layouts/page_medium.uim:187
msgid "Volume Label:"
msgstr ""

#: gui/layouts/page_medium.uim:188
msgid "The 'label' given to the created medium"
msgstr ""

#: gui/layouts/page_medium.uim:195
msgid "The length may not exceed 16 bytes, 11 for vfat"
msgstr ""

#: gui/layouts/page_medium.uim:201
msgid "Enter a new label for the volume, empty to use default"
msgstr ""

#: gui/layouts/page_medium.uim:208
msgid "Write the larch medium"
msgstr ""

#: gui/layouts/page_medium.uim:209
msgid "The larch image will be written to the 'iso' file or to the partition, as selected"
msgstr ""

#: gui/layouts/page_medium.uim:217
msgid "Choose unmounted partition"
msgstr ""

#: gui/layouts/page_medium.uim:218
msgid "Select larch source partition"
msgstr ""

#: gui/layouts/page_medium.uim:219
msgid ""
"Device to receive larch system\n"
"WARNING: Be very careful in choosing here,\n"
"if you choose the wrong one you might\n"
"seriously damage your system!"
msgstr ""

#: gui/layouts/page_medium.uim:223
msgid "Invalid larch medium folder: %s"
msgstr ""

#: gui/layouts/page_medium.uim:224
msgid "Volume label (clear to use default):"
msgstr ""

#: gui/layouts/page_medium.uim:225
msgid "Save 'iso' to ..."
msgstr ""

#: gui/layouts/page_medium.uim:226
msgid "Read 'iso' file"
msgstr ""

#: gui/layouts/page_medium.uim:232
msgid "Device (e.g. /dev/sdb1)"
msgstr ""

#: gui/layouts/page_medium.uim:233
msgid "Search (for 'larchboot' file)"
msgstr ""

#: gui/layouts/page_medium.uim:239
msgid "writeable medium"
msgstr ""

#: gui/layouts/page_medium.uim:241
msgid "boot iso"
msgstr ""

#: gui/layouts/page_medium.uim:243
msgid "larchify output"
msgstr ""

#: gui/layouts/page_medium.uim:244
msgid "other medium"
msgstr ""

#: gui/layouts/page_medium.uim:245
msgid "iso file"
msgstr ""

#: gui/layouts/page_project.uim:37
msgid "Profile"
msgstr ""

#: gui/layouts/page_project.uim:48
msgid "Select:"
msgstr ""

#: gui/layouts/page_project.uim:54
msgid "Choose a profile from those already in your larch working folder"
msgstr ""

#: gui/layouts/page_project.uim:60
msgid "Browse for Profile"
msgstr ""

#: gui/layouts/page_project.uim:61
msgid "Fetch a profile from the file-system"
msgstr ""

#: gui/layouts/page_project.uim:66
msgid "Clone"
msgstr ""

#: gui/layouts/page_project.uim:67
msgid "Make a copy of the current profile under a new name"
msgstr ""

#: gui/layouts/page_project.uim:72
msgid "Rename"
msgstr ""

#: gui/layouts/page_project.uim:73
msgid "Rename the current profile"
msgstr ""

#: gui/layouts/page_project.uim:78 gui/layouts/page_project.uim:132
msgid "Delete"
msgstr ""

#: gui/layouts/page_project.uim:79
msgid "Delete an unused profile"
msgstr ""

#: gui/layouts/page_project.uim:84
msgid "Copy to ..."
msgstr ""

#: gui/layouts/page_project.uim:85
msgid "Copy the current profile to somehere else"
msgstr ""

#: gui/layouts/page_project.uim:92
msgid "Advanced Project Options"
msgstr ""

#: gui/layouts/page_project.uim:116
msgid "Choose Existing Project:"
msgstr ""

#: gui/layouts/page_project.uim:120
msgid "Choose a project from those already defined"
msgstr ""

#: gui/layouts/page_project.uim:126
msgid "New Project"
msgstr ""

#: gui/layouts/page_project.uim:127
msgid "Create a new project"
msgstr ""

#: gui/layouts/page_project.uim:133
msgid "Delete a project"
msgstr ""

#: gui/layouts/page_project.uim:138
msgid "Installation Path"
msgstr ""

#: gui/layouts/page_project.uim:146
msgid "The root directory of the Arch installation to larchify"
msgstr ""

#: gui/layouts/page_project.uim:152
msgid "Change the root directory of the Arch installation"
msgstr ""

#: gui/layouts/page_project.uim:159
msgid "Select profile source folder"
msgstr ""

#: gui/layouts/page_project.uim:160
msgid "Destination profile exists - replace it?"
msgstr ""

#: gui/layouts/page_project.uim:161
msgid "Enter new name for current profile:"
msgstr ""

#: gui/layouts/page_project.uim:162
msgid "Profile '%s' exists already"
msgstr ""

#: gui/layouts/page_project.uim:163
msgid "Can't rename the profile, it is in use by other projects"
msgstr ""

#: gui/layouts/page_project.uim:165
msgid "Save profile folder"
msgstr ""

#: gui/layouts/page_project.uim:166
msgid "Destination exists - replace it?"
msgstr ""

#: gui/layouts/page_project.uim:167
msgid "Select the profile for deletion"
msgstr ""

#: gui/layouts/page_project.uim:168
msgid "Remove Profile"
msgstr ""

#: gui/layouts/page_project.uim:169
msgid "There are no profiles which can be deleted - all are in use"
msgstr ""

#: gui/layouts/page_project.uim:171
msgid "Couldn't delete profile '%s' - check permissions"
msgstr ""

#: gui/layouts/page_project.uim:173
msgid ""
"An empty path here will reset to the default.\n"
"  WARNING: Double check your path -\n"
"  If you make a mistake here it could destroy your system!\n"
"\n"
"Enter new installation path:"
msgstr ""

#: gui/layouts/page_project.uim:177
msgid "Enter name for new project:"
msgstr ""

#: gui/layouts/page_project.uim:178
msgid "Project '%s' already exists"
msgstr ""

#: gui/layouts/page_project.uim:179
msgid "Select the project for deletion"
msgstr ""

#: gui/layouts/page_project.uim:180
msgid "Remove Project"
msgstr ""

#: gui/layouts/page_project.uim:181
msgid "There are no projects which can be deleted"
msgstr ""

#: gui/layouts/page_project.uim:182
msgid "'%s' is not a profile folder"
msgstr ""

#: gui/layouts/page_project.uim:183
msgid "The path '%s' is already in use, not saving"
msgstr ""

#: gui/layouts/page_project.uim:184
msgid "Enter the name for the new profile:"
msgstr ""

#: gui/layouts/profile_browse.uim:43
msgid ""
"You can browse the file-system for the source profile directory,\n"
"or else choose one of the examples"
msgstr ""

#: gui/layouts/profile_browse.uim:48
msgid "Example Profiles"
msgstr ""

#: gui/layouts/profile_browse.uim:49
msgid "Here you can select one of the example profiles to use as a starting point"
msgstr ""

#: gui/layouts/profile_browse.uim:60
msgid "Browse file-system"
msgstr ""

#: gui/layouts/profile_browse.uim:61
msgid "Open a file dialog to search for the profile"
msgstr ""

#: gui/layouts/profile_browse.uim:66
msgid "Source:"
msgstr ""

#: gui/layouts/profile_browse.uim:71
msgid "The path from which the profile directory will be copied"
msgstr ""

#: gui/layouts/profile_browse.uim:75
msgid "New name:"
msgstr ""

#: gui/layouts/profile_browse.uim:80
msgid "The name the profile will be given in the work area"
msgstr ""

#: gui/layouts/profile_browse.uim:85
msgid "Open a dialog to change the new profile's name"
msgstr ""

#: gui/layouts/progress.uim:38
msgid "Processing ..."
msgstr ""

#: gui/layouts/progress.uim:49
msgid "An indication of the progress of the current operation, if possible"
msgstr ""

#: gui/layouts/progress.uim:55
msgid "Stop the current action"
msgstr ""

#: gui/layouts/progress.uim:60
msgid "Done"
msgstr ""

